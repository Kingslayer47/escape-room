import random
import simple_colors
from string import ascii_letters
import asyncio
import sqlite3
import sys
from time import sleep

database = sqlite3.connect('game.db')


def idGenrator(limit):
    return ''.join(random.choice(ascii_letters+'123456789') for letter in range(limit))

class roomInitalisation():

    def __init__(self,player):
        self.description_1 = f""
        self.player = player
        self.lockIds = [idGenrator(6), idGenrator(6), idGenrator(6)]
        print(simple_colors.cyan("Building Rooms...\n"))
        asyncio.run(asyncio.sleep(1))
        print(simple_colors.cyan("Adding Items to Rooms...\n"))
        asyncio.run(asyncio.sleep(1))
        self.roomItems = [
                    {
                        'item':'lockPicker',
                        'name':'Lock Picker in a very bad condition',
                        'quantity':'1',
                        'room':1
                    }, 
                    {
                        'item':'battery',
                        'name':'12V battery',
                        'quantity':'1',
                        'room':1
                    }, 
                    {
                        'item':'keyDoor',
                        'name':f'Silver Key with id = {self.lockIds[0]}',
                        'quantity':'1',
                        'room':1,
                    }, 
                    {
                        'item':'lockDoor',
                        'name':f'Brown door with lock id = {self.lockIds[0]}',
                        'quantity':'1',
                        'room':1
                    },
                    {
                        'item':'lockVent',
                        'name':f'Vent with lock id = {self.lockIds[1]}',
                        'quantity':'1',
                        'room':1,
                        'condition':100
                    },
                    {
                        'item':'note',
                        'name':f'A Note that reads : {simple_colors.yellow("I had to hide the emergency supplies in the safe behind the old painting")}',
                        'quantity':'1',
                        'room':1
                    }
                    ]
        self.itemEntry()
        self.step = 0

    def itemEntry(self):
        cursor = database.cursor()
        for item in self.roomItems:
            cursor.execute(f"""INSERT INTO items (itemId, name, quantity, room, ownerId, sessionId) VALUES (?, ?, ?, ?, ?, ?)""",[item.get('item'), item.get('name'), item.get('quantity'), item.get('room'), f"ROOM {item.get('room')}", self.player.sessionId])
            print(f"Added {simple_colors.green(item.get('item'))} to Database")
            asyncio.run(asyncio.sleep(.059))
            
        database.commit()


    def queryEntry(self,verificationQuery : str ,actualQuery : str):
        queryInput = input("\nSQL :: ")
        if queryInput.startswith('.'):
            self.queryProcess(queryInput,True)
            if queryInput == actualQuery:
                return True
            return False
        else:
            while True:
                if queryInput.lower().replace(' ','').replace("'",'').replace('"','') == verificationQuery.lower().replace(' ','').replace("'",'').replace('"',''):
                    self.queryProcess(queryInput,False,[actualQuery])
                    return True
                else:
                    print(simple_colors.red("The input query is incorrect, please try again."))
                    return False    
                
    
    def queryProcess(self, query, command=False, verificationQuery=[]):
        cursor = database.cursor()
        if command:
            if query.lower().startswith('.table'):
                print(f'\nTABLES THAT CAN BE ACCESSED ARE : \n{simple_colors.red("CHARACTER",["dim","bold"])} : {simple_colors.magenta("Display characters in current game session.",["bright","bold","italic"])}\n{simple_colors.red("ITEMS",["dim","bold"])} : {simple_colors.magenta("Displays the ITEMS in the game.",["bright","bold","italic"])}')
            elif query.lower().startswith('.help'):
                print(f"""
    {simple_colors.blue('.schema')} : {simple_colors.green('Shows schema for the tables.')}
    {simple_colors.blue('.table')} : {simple_colors.green('Shows the tables available in database.')}
    {simple_colors.blue('.inventory')} : {simple_colors.green('Shows the inventory for the character')}
    {simple_colors.blue('.character')} : {simple_colors.green('Shows the character stats and details')}
    {simple_colors.blue('.help')} : {simple_colors.green('Shows this message')}
    {simple_colors.blue('.quit')} : {simple_colors.green('Quits the game')}

    """)
            elif query.lower().startswith('.schema'):
                print(simple_colors.red("""
    TABLE character (entry INTEGER PRIMARY KEY, name STRING, hp INTEGER, room INTEGER, charid STRING, validQuery STRING);    
    TABLE items (entry INTEGER PRIMARY KEY, itemId STRING, name STRING, quantity INTEGER, room STRING, ownerId STRING);
                    """))
            elif query.lower().startswith('.inventory'):
                print(f"{simple_colors.blue('QUERY EXECUTED')} :: {simple_colors.green('SELECT * FROM items WHERE ownerId = '+self.player.characterId)}")
                cursor.execute("""SELECT * FROM items WHERE ownerId = ?""",[self.player.characterId])
                details = cursor.fetchall()
                displayString = "    ENTRY  |    ITEM    |    QUANTITY    |    DESCRIPTION\n"
                for items in range(len(details)):
                    displayString += simple_colors.red(f"\n    {items+1}    |    {details[items][1]}    |    {details[items][3]}    |    {details[items][2]}    \n")
                if len(details) == 0:
                    displayString = simple_colors.red("\nInventory is empty\n",["bold","bright"])
                print(displayString)
            elif query.lower().startswith('.character'):
                print(simple_colors.green("Fetching your character details...\n",['bright','bold']))
                asyncio.run(asyncio.sleep(1))
                print(f'\nName : {simple_colors.cyan(self.player.name)}\nHealth : {simple_colors.cyan(self.player.hp)}\nCurrent Room : {simple_colors.cyan(self.player.room)}\nCharacter ID : {simple_colors.cyan(self.player.characterId)}\nObjective : {simple_colors.cyan(self.player.objective)}')
            elif query.lower().startswith('.quit'):
                exit(0)
        else:
            for queries in verificationQuery:
                cursor.execute(queries)
            database.commit()
            print(f"\n {simple_colors.green('Query Successfully Executed!')}\n")
        
    def steps(self):
        if self.player.step == 0:
            room1Des = f"""\n                                         You Enter : {simple_colors.red('The Room')}                           \n
                        {simple_colors.magenta('Room Description :',['bright','italic'])} You enter the room via {simple_colors.green('green door',['bright'])}...
                          you are standing at the upper left side of the room
                              The room has {simple_colors.cyan('Cyan Walls')}, with a window 
            You see a {simple_colors.green('brown door',['bright'])} at upper right , {simple_colors.yellow('which seems to be the way to the next room',['italic'])}
                      there's a single bed at bottom right, with a {simple_colors.red('lock picker',['bold'])} on it
                a {simple_colors.magenta('fireplace')} in the center top of the room, {simple_colors.yellow("there is something shiny inside it.",['italic'])}
    a chair on some distance to the left of the bed , there's a {simple_colors.green('vent',['bright'])} just above the chair {simple_colors.yellow('with a lock on it',['italic'])}
                        just above the {simple_colors.magenta('fireplace')} there's an {simple_colors.red('old painting',['bold'])}.  
            """
            for char in room1Des:
                if char != " ":
                    asyncio.run(asyncio.sleep(.05))
                sys.stdout.write(char)
                sys.stdout.flush()
            
            print(f"\n{simple_colors.blue('HINT : To view schema of items TABLE type ')}",simple_colors.red('.schema'))
            self.player.objective = f"Pick up the item with itemId as {simple_colors.red('lockPicker')} by setting it's {simple_colors.blue('ownerId')} to {simple_colors.green(self.player.characterId)} in {simple_colors.yellow('items')} Table"
            self.player.step = 1
        elif self.player.step == 1:
            self.player.solution = f"UPDATE items SET ownerId = {self.player.characterId} WHERE itemId = lockPicker"
            while True:
                print(f"\n\nCurrent Objective :: {self.player.objective}\n\n")
                next = self.queryEntry(self.player.solution,f"""UPDATE items SET ownerId = '{self.player.characterId}' WHERE itemId = 'lockPicker' AND sessionId = '{self.player.sessionId}'""")
                if next:
                    break
            self.player.step += 1
        elif self.player.step == 2:
            print(f"\n{simple_colors.green('You picked up the lockPicker',['bright','bold'])}\n")
            self.player.objective = simple_colors.yellow('Check your inventory.\n')
            print(simple_colors.red('By typing'),simple_colors.blue(' .help'),simple_colors.red(' you can access the help menu. \n'))
            while True:
                print(f"\n\nCurrent Objective :: {self.player.objective}\n\n")
                next = self.queryEntry('.inventory','.inventory')
                if next:
                    break
            self.player.step += 1
            
        elif self.player.step == 3:
            print(f"\n Note :: {simple_colors.red('''Using an item consumes one of it's quantity, 0 quantity means the item cannot be used again.''')}")
            self.player.objective = simple_colors.yellow('Use lockPicker to open vent lock.')
            print("To use an item , reduce it's quantity by 1 ")
            while True:
                print(f"\n\nCurrent Objective :: {self.player.objective}\n\n")
                next = self.queryEntry(f'UPDATE items SET quantity = quantity-1 WHERE ownerId = {self.player.characterId} AND itemId = lockPicker',f'UPDATE items SET quantity = quantity-1 WHERE ownerId = "{self.player.characterId}" AND itemId = "lockPicker"')
                if next:
                    break
            self.player.step += 1
        elif self.player.step == 4:
            print("You used lockPicker to open the vent ")
            print(f"\nYou found a note, to view what it has on it {simple_colors.red('SELECT')} it's {simple_colors.green('name')} from {simple_colors.blue('items')} TABLE where {simple_colors.green('itemID')} is {simple_colors.yellow('note')}")
            self.player.objective = f"View what does note have written on it {simple_colors.red('SELECT')} it's {simple_colors.green('name')} from {simple_colors.blue('items')} TABLE where {simple_colors.green('itemID')} is {simple_colors.yellow('note')}"
            while True:
                print(f"\n\nCurrent Objective :: {self.player.objective}\n\n")
                next = self.queryEntry('SELECT name FROM items WHERE itemId = note',f'SELECT name FROM items WHERE itemId = "note"')
                if next:
                    print(self.roomItems[-1].get('name'))
                    break
            self.player.step = 5
        elif self.player.step == 5:
            print(simple_colors.cyan("\nUpon reading the note you approach the painting and, remove it.\nA key falls infront of you, using that key you unlock the the safe inside the safe you see a label that say's owner : ROOM 1\n"))
            self.player.objective = simple_colors.yellow("Check for items that belong to ROOM 1")
            while True:
                print(f"\n\nCurrent Objective :: {self.player.objective}\n\n")
                next = self.queryEntry('SELECT * FROM items WHERE ownerId = "ROOM 1"',f'SELECT * FROM items WHERE ownerId = "ROOM 1"')
                if next:
                    cursor = database.cursor()
                    cursor.execute("""SELECT * FROM items WHERE ownerId = ? AND sessionId = ?""",["ROOM 1",self.player.sessionId])
                    details = cursor.fetchall()
                    displayString = "    ENTRY  |    ITEM    |    QUANTITY    |    DESCRIPTION\n"
                    for items in range(len(details)):
                        displayString += simple_colors.red(f"\n    {items+1}    |    {details[items][1]}    |    {details[items][3]}    |    {details[items][2]}    \n")
                    print(displayString)
                    break
            self.player.step = 6
        elif self.player.step == 6:
            print("\nNow you know all the items that are present in the room , try to change their ownerId to your characterId\n")
            self.player.objective = simple_colors.yellow("Change ownerId of every item in 'ROOM 1' to you characterId")
            while True:
                print(f"\n\nCurrent Objective :: {self.player.objective}\n\n")
                next = self.queryEntry(f'UPDATE items SET ownerId = {self.player.characterId} WHERE ownerId = "ROOM 1"',f'UPDATE items SET ownerId = "{self.player.characterId}" WHERE ownerId = "ROOM 1" AND sessionId = "{self.player.sessionId}"')
                if next:
                    break
            self.player.step = 7
        elif self.player.step == 7:
            
            self.player.objective = simple_colors.yellow("Check your inventory")
            while True:
                print(f"\n\nCurrent Objective :: {self.player.objective}\n\n")
                next = self.queryEntry(f'.inventory',f'.inventory')
                if next:
                    break
            self.player.step += 1
        elif self.player.step == 8:
            print("\nAfter executing the query you come to learn that you now have the key to the door that seems to be the way out\n")

            print("\nNow that you have the keyDoor in your inventory, open the door.")

            print(simple_colors.red("\nTO OPEN lockDoor DELETE IT FROM items WITH IT'S ownerId\n",['bright']))
            self.player.objective = simple_colors.yellow("\nDELETE lockDoor From your inventory.\n")
            while True:
                print(f"\n\nCurrent Objective :: {self.player.objective}\n\n")
                next = self.queryEntry(f'DELETE FROM items WHERE itemId = lockDoor AND ownerId = {self.player.characterId}',f'DELETE FROM items WHERE itemId = "lockDoor" AND ownerId = "{self.player.characterId}"')
                if next:
                    break
            self.player.step += 1
        elif self.player.step == 9:
            print(f"""You now see a note on the other side of the door , that says {simple_colors.yellow("The room has an insert point , you can insert a car in this room using the magic of SQL")}""")
            self.player.objective = simple_colors.yellow("\nINSERT a CAR into 'ROOM 2' by setting it's ownerid to 'ROOM 2' AND itemid to 'car' \n")
            while True:
                print(f"\n\nCurrent Objective :: {self.player.objective}\n\n")
                next = self.queryEntry(f'INSERT INTO items (ownerid, itemid) VALUES ("Room 2", car)',f'INSERT INTO items (ownerid, itemid, room, quantity, sessionId, name) VALUES ("Room 2", "car", "2", 1, "{self.player.sessionId}", "A red super car spawns infront of you")')
                if next:
                    break
            self.player.step += 1
        elif self.player.step == 10:
            print("\nThe garage shutter opens , it's a sign to use the car and flee away.\n")
            self.player.objective = simple_colors.yellow("Use the item with itemid car by reducing it's quantity by 1")
            while True:
                print(f"\n\nCurrent Objective :: {self.player.objective}\n\n")
                next = self.queryEntry(f'UPDATE items SET quantity = quantity - 1 WHERE itemid = car',f'UPDATE items SET quantity = quantity - 1 WHERE itemid = "car" AND sessionId = "{self.player.sessionId}"')
                if next: 
                    break
            self.player.step += 1
        elif self.player.step ==  11:
            print(simple_colors.green("As soon as you sat in the car and was about to drive it, you woke up from the fantastic dream you were having. ;)"))
            print(f"In the end you didn't escape the room :O, what a loser lmao XD")
            return False
        else:
            print("Wait how , did you do that ,  well you won the game I guess, damn! you smart.")
            print("Congrats!!")
            return False
