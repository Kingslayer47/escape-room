from rooms import *


class character():

    def __init__(self):
        self.name = input("Enter your username : ")
        self.hp = 100
        self.room = 1
        self.roomStatus = {1:'locked', 2:'locked', 3:'locked'}
        self.validQuery = None
        self.characterId = idGenrator(12)
        self.sessionId = idGenrator(25)
        self.objective = None
        self.solution = None
        self.step = 0
        
        self.addCharacter()

    def addCharacter(self):
        cursor = database.cursor()
        cursor.execute("INSERT INTO character (name, hp, room, charid, validQuery) VALUES (?, ?, ?, ?, ?)",[self.name, self.hp, self.room, self.characterId, ""])
        database.commit()

    
    def queryProcess(self, query):
        databaseCursor = database.cursor()
        try:
            databaseCursor.execute(query)
            databaseCursor.close()
            self.queryLog(query)
            return (0,"Valid Query")
        except:
            return (1,"Invalid Query")